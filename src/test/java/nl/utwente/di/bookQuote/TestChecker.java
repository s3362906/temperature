package nl.utwente.di.bookQuote;

import nl.utwente.di.fahrenheit.checker;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestChecker {
    @Test
    public void testFahrenheit() throws Exception {
        checker checker1 = new checker();
        double degree = checker1.checkFahrenheit(10.0);
        Assertions.assertEquals(10.0,degree,50.0,"Fahrenheit value");
    }
}
